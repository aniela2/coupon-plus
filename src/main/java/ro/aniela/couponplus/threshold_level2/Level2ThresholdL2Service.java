package ro.aniela.couponplus.threshold_level2;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import org.hibernate.id.enhanced.LegacyNamingStrategy;
import org.springframework.stereotype.Service;
import ro.aniela.couponplus.treshold.ThresholdRepository;
import ro.aniela.couponplus.treshold.ThresholdService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class Level2ThresholdL2Service {
    private final Level2ThresholdL2Repository level2ThresholdL2Repository;
    private final Level2ThresholdL2ToDtoMapper level2ThresholdL2ToDtoMapper;
    private final ThresholdRepository thresholdRepository;

    public Level2ThresholdL2Dto getLevel2ThresholdId(Integer id) {
        return level2ThresholdL2ToDtoMapper.apply(level2ThresholdL2Repository.getReferenceById(id));
    }

    public List<Level2ThresholdL2Dto> getAllLevel2Threshold() {
        return level2ThresholdL2Repository.findAll().stream().map(c -> level2ThresholdL2ToDtoMapper.apply(c)).toList();
    }

    public Level2ThresholdL2Dto saveLevel(Level2ThresholdL2Dto level2ThresholdL2Dto) {
        Level2ThresholdL2 level2ThresholdL2 = new Level2ThresholdL2();
        level2ThresholdL2.setThreshold(thresholdRepository.getReferenceById(level2ThresholdL2Dto.thresholdId()));
        level2ThresholdL2Repository.save(level2ThresholdL2);
        return level2ThresholdL2ToDtoMapper.apply(level2ThresholdL2);
    }

    public Level2ThresholdL2Dto updateLevel(Integer id, Level2ThresholdL2Dto level2ThresholdL2Dto) {
        Level2ThresholdL2 level2ThresholdL2 = level2ThresholdL2Repository.getReferenceById(id);
        level2ThresholdL2.setThreshold(thresholdRepository.getReferenceById(level2ThresholdL2Dto.thresholdId()));
        level2ThresholdL2Repository.save(level2ThresholdL2);
        return level2ThresholdL2ToDtoMapper.apply(level2ThresholdL2);
    }

    public void deleteLevel2(Integer id) {
        level2ThresholdL2Repository.deleteById(id);
    }
}
