package ro.aniela.couponplus.threshold_level2;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class Level2ThresholdL2ToDtoMapper implements Function<Level2ThresholdL2, Level2ThresholdL2Dto> {
    public Level2ThresholdL2Dto apply(Level2ThresholdL2 thresholdL2){
        return new Level2ThresholdL2Dto(thresholdL2.getId(), thresholdL2.getThreshold().getId());
    }

}
