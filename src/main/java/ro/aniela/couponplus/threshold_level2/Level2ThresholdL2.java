package ro.aniela.couponplus.threshold_level2;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.couponplus.treshold.Threshold;

@Entity(name = "ThresholdL2")
@Table(name = "threshold_l2")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Level2ThresholdL2 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "threshold_id", nullable = false)
    private Threshold threshold;
}
