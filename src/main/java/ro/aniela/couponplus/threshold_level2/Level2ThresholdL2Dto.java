package ro.aniela.couponplus.threshold_level2;

public record Level2ThresholdL2Dto(Integer id, Integer thresholdId) {
}
