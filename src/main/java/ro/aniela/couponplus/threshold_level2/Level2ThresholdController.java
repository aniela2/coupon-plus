package ro.aniela.couponplus.threshold_level2;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/thresholds-l2")
@RequiredArgsConstructor
public class Level2ThresholdController {
    private final Level2ThresholdL2Service level2ThresholdL2Service;

    @GetMapping("/{id}")
    public ResponseEntity<Level2ThresholdL2Dto> getLeve2ById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(level2ThresholdL2Service.getLevel2ThresholdId(id));
    }

    @GetMapping
    public ResponseEntity<List<Level2ThresholdL2Dto>> getLevel2() {
        return ResponseEntity.ok(level2ThresholdL2Service.getAllLevel2Threshold());
    }

    @PostMapping
    public ResponseEntity<Level2ThresholdL2Dto> saveLevel2(@RequestBody Level2ThresholdL2Dto level2ThresholdL2Dto) {
        return new ResponseEntity(level2ThresholdL2Service.saveLevel(level2ThresholdL2Dto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Level2ThresholdL2Dto> updateLevel2(@PathVariable("id") Integer id,
                                                             @RequestBody Level2ThresholdL2Dto level2ThresholdL2Dto) {
        return ResponseEntity.ok(level2ThresholdL2Service.updateLevel(id, level2ThresholdL2Dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLevel2ById(@PathVariable("id") Integer id) {
        level2ThresholdL2Service.deleteLevel2(id);
        return ResponseEntity.noContent().build();
    }


}
