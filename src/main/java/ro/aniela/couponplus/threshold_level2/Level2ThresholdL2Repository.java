package ro.aniela.couponplus.threshold_level2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Level2ThresholdL2Repository extends JpaRepository<Level2ThresholdL2, Integer> {
}
