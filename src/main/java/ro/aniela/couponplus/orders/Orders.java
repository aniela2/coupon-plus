package ro.aniela.couponplus.orders;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.couponplus.buyer.Buyer;
import ro.aniela.couponplus.product_order.ProductOrder;

import java.time.LocalDate;
import java.util.List;

@Entity(name = "Orders")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "ticket_no", nullable = false)
    private Integer ticketNo;
    @Column(name = "tms", nullable = false)
    private LocalDate tms;
    @JoinColumn(name = "buyer_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Buyer buyer;
    @OneToMany(cascade = CascadeType.ALL, mappedBy ="orders")
    private List<ProductOrder> productOrders;


}
