package ro.aniela.couponplus.orders;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.DeclareError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/orders")
@RequiredArgsConstructor
public class OrdersController {
    private final OrdersService ordersService;

    @GetMapping("/{id}")
    private ResponseEntity<OrdersDto> getOrdersById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(ordersService.getOrdersById(id));
    }

    @GetMapping
    private ResponseEntity<List<OrdersDto>> getAllOrders() {
        return ResponseEntity.ok(ordersService.getAllOrders());
    }

    @PostMapping
    public ResponseEntity<OrdersDto> saveOrders(@RequestBody OrdersDto ordersDto) {
        return new ResponseEntity(ordersService.saveOrders(ordersDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<OrdersDto> updateOrder(@PathVariable("id") Integer id, @RequestBody OrdersDto ordersDto) {
        return ResponseEntity.ok(ordersService.updateOrders(id, ordersDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrdersById(@PathVariable("id") Integer id) {
        ordersService.deleteOrderById(id);
        return ResponseEntity.noContent().build();
    }
}
