package ro.aniela.couponplus.orders;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class OrdersToDtoMapper implements Function<Orders, OrdersDto> {
    public OrdersDto apply(Orders orders) {
        return new OrdersDto(orders.getId(), orders.getTicketNo(), orders.getTms(), orders.getBuyer().getId());
    }
}
