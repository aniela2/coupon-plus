package ro.aniela.couponplus.orders;

import java.time.LocalDate;

public record OrdersDto(Integer id, Integer ticketNo, LocalDate tms, Integer buyerId) {

}
