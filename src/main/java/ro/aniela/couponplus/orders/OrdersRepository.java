package ro.aniela.couponplus.orders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.function.Function;
@Repository
public interface OrdersRepository extends JpaRepository<Orders, Integer> {

}
