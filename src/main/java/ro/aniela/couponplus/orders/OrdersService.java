package ro.aniela.couponplus.orders;

import jakarta.persistence.criteria.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.couponplus.buyer.BuyerRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrdersService {

    private final OrdersToDtoMapper ordersToDtoMapper;
    private final OrdersRepository ordersRepository;
    private final BuyerRepository buyerRepository;

    public OrdersDto getOrdersById(Integer id) {
        return ordersToDtoMapper.apply(ordersRepository.getReferenceById(id));
    }

    public List<OrdersDto> getAllOrders() {
        return ordersRepository.findAll().stream().map(o -> ordersToDtoMapper.apply(o)).toList();
    }

    public OrdersDto saveOrders(OrdersDto ordersDto) {
        Orders orders = new Orders();
        orders.setTicketNo(ordersDto.ticketNo());
        orders.setTms(ordersDto.tms());
        orders.setBuyer(buyerRepository.getReferenceById(ordersDto.buyerId()));
        ordersRepository.save(orders);
        return ordersToDtoMapper.apply(orders);
    }

    public OrdersDto updateOrders(Integer id, OrdersDto ordersDto) {
        Orders orders = ordersRepository.getReferenceById(id);
        orders.setTicketNo(ordersDto.ticketNo());
        orders.setTms(ordersDto.tms());
        orders.setBuyer(buyerRepository.getReferenceById(ordersDto.buyerId()));
        ordersRepository.save(orders);
        return ordersToDtoMapper.apply(orders);
    }

    public void deleteOrderById(Integer id) {
        ordersRepository.deleteById(id);
    }
}
