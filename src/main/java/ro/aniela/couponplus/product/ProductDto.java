package ro.aniela.couponplus.product;

import ro.aniela.couponplus.productcategory.ProductCategory;

public record ProductDto(Integer id, String name, Double price, Double weight, Integer productCategoryId) {

}
