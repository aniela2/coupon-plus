package ro.aniela.couponplus.buyer;

public record BuyerDto(Integer id, String name, Integer age, String address, Integer phone, String email) {

}
