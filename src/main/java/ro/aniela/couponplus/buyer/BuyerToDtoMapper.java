package ro.aniela.couponplus.buyer;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BuyerToDtoMapper implements Function<Buyer, BuyerDto> {
    public BuyerDto apply(Buyer buyer) {
        return new BuyerDto(buyer.getId(), buyer.getName(), buyer.getAge(), buyer.getAddress(), buyer.getPhone(), buyer.getEmail());
    }
}
