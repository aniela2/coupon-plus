package ro.aniela.couponplus.buyer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.couponplus.coupon.Coupon;
import ro.aniela.couponplus.orders.Orders;

import java.util.List;

@Entity(name = "Buyer")
@Table(name = "buyer")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Buyer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "age", nullable = false)
    private Integer age;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "phone", nullable = false)
    private Integer phone;
    @Column(name = "email", nullable = false)
    private String email;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "buyer")
    private List<Coupon> coupons;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "buyer")
    private List<Orders> orders;
}
