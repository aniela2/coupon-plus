package ro.aniela.couponplus.buyer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BuyerService {
    private final BuyerRepository buyerRepository;
    private final BuyerToDtoMapper buyerToDtoMapper;

    public BuyerDto getBuyer(Integer id) {
        return buyerToDtoMapper.apply(buyerRepository.getReferenceById(id));
    }

    public List<BuyerDto> getAllBuyers() {
        return buyerRepository.findAll().stream().map(b -> buyerToDtoMapper.apply(b)).toList();
    }

    public BuyerDto saveBuyer(BuyerDto buyerDto) {
        Buyer buyer = new Buyer();
        buyer.setName(buyerDto.name());
        buyer.setAge(buyerDto.age());
        buyer.setAddress(buyerDto.address());
        buyer.setPhone(buyerDto.phone());
        buyer.setEmail(buyerDto.email());
        buyerRepository.save(buyer);
        return buyerToDtoMapper.apply(buyer);
    }

    public BuyerDto update(Integer id, BuyerDto buyerDto) {
        Buyer buyer = buyerRepository.getReferenceById(id);
        buyer.setName(buyerDto.name());
        buyer.setAge(buyerDto.age());
        buyer.setAddress(buyerDto.address());
        buyer.setPhone(buyerDto.phone());
        buyer.setEmail(buyerDto.email());
        buyerRepository.save(buyer);
        return buyerToDtoMapper.apply(buyer);
    }

    public void deleteBuyerById(Integer id) {
        buyerRepository.deleteById(id);

    }

}
