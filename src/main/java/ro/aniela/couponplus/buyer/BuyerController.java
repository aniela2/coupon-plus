package ro.aniela.couponplus.buyer;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/buyers")
@RequiredArgsConstructor
public class BuyerController {
    private final BuyerService buyerService;

    @GetMapping("/{id}")
    public ResponseEntity<BuyerDto> getBuyer(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(buyerService.getBuyer(id));
    }

    @GetMapping
    public ResponseEntity<List<BuyerDto>> getAllBuyer() {
        return ResponseEntity.ok(buyerService.getAllBuyers());
    }

    @PostMapping
    public ResponseEntity<BuyerDto> saveBuyer(@RequestBody BuyerDto buyerDto) {
        return new ResponseEntity(buyerService.saveBuyer(buyerDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BuyerDto> updateBuyer(@PathVariable("id") Integer id, @RequestBody BuyerDto buyerDto) {
        return ResponseEntity.ok(buyerService.update(id, buyerDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBuyer(@PathVariable("id") Integer id) {
        buyerService.deleteBuyerById(id);
        return ResponseEntity.noContent().build();
    }
}
