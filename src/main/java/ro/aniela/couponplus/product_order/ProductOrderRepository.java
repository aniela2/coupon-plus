package ro.aniela.couponplus.product_order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductOrderRepository extends JpaRepository<ProductOrder, Integer> {
    @Query(value = """
            select po from ProductOrder po where po.orders.id=:orderId
              """)
    List<ProductOrder> productOrdersByOrderId(@Param("orderId") Integer orderId);

    @Query(value = """
            select new ro.aniela.couponplus.product_order.ProductOrderDto(po.orders.id, po.product.id,po.quantity) 
                       from ProductOrder po, Product p
                        where po.product.id = p.id
                       and po.product.id=:productId
            """)
    List<ProductOrderDto> productOrderByProductId(@Param("productId") Integer productId);

    @Query(value = """
            select new ro.aniela.couponplus.product_order.ProductOrderDto(po.orders.id, po.product.id, po.quantity)
            from ProductOrder po, Product p, Orders o
            where po.product.id = p.id
            and po.orders.id = o.id
            and po.product.id = :productId and
            po.orders.id = :orderId
            """
    )
    ProductOrderDto getPoByOrderIdAndProductId(@Param("orderId") Integer orderId, @Param("productId") Integer productId);


//    Optional<ProductOrderDto> getProductOrderByOrdersIdAndProductId(@Param("ordersId") Integer ordersId, @Param("productId") Integer productId);

    @Query(value = """
            select po 
            from ProductOrder po 
            """)
    List<ProductOrder> getAllPO();

    @Query(value = """
            select * 
            from product_order 
             """, nativeQuery = true)
    List<ProductOrder> getAllProductOrders();

    @Query(value = """
            select new ro.aniela.couponplus.product_order.ProductOrderDto(po.orders.id, po.product.id, po.quantity)
            from ProductOrder po
             """)
    List<ProductOrderDto> getAllProdOrders();

    @Transactional
    @Modifying
    @Query(value = """
            update ProductOrder po
            set po.quantity= :quantity
            where po.orders.id= :orderId
            and po.product.id= :productId
            """)
    void updateProductOrdersQuantity(@Param("orderId") Integer orderId, @Param("productId") Integer productId, @Param("quantity") Double quantity);

    @Transactional
    @Modifying
    void deleteProductOrderByOrdersId(@Param("orderId") Integer orderId);

    @Transactional
    @Modifying
    @Query("""
            delete ProductOrder po
            where po.orders.id=:orderId
            and po.product.id = :productId
            """)
    void deletePOByOrderIdAndProductId(@Param("orderId") Integer orderId, @Param("productId") Integer productId);

}






