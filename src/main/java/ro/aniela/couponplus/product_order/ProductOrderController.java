package ro.aniela.couponplus.product_order;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/product-orders")
//@RequiredArgsConstructor
public class ProductOrderController {

    @Autowired
    private ProductOrderService productOrderService;

    @GetMapping("/order/{id}")
    public ResponseEntity<List<ProductOrderDto>> getPoByOrder(@PathVariable("id") Integer ordersId) {
        return ResponseEntity.ok(productOrderService.getPoByOrder(ordersId));
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<List<ProductOrderDto>> getPoByProduct(@PathVariable("id") Integer productId) {
        return ResponseEntity.ok(productOrderService.getPoByProduct(productId));
    }

    @GetMapping("/order/{orderId}/product/{productId}")
    public ResponseEntity<ProductOrderDto> getPOBByOrderAndProduct(@PathVariable("orderId") Integer orderId, @PathVariable("productId") Integer productId) {
        return ResponseEntity.ok(productOrderService.getPoByOrderAndProduct(orderId, productId));
    }

    @GetMapping
    public ResponseEntity<List<ProductOrderDto>> getAllPO() {
        return ResponseEntity.ok(productOrderService.getAllPo());
    }

    @PostMapping
    public ResponseEntity<ProductOrderDto> savePo(@RequestBody ProductOrderDto productOrderDto) {
        return new ResponseEntity(productOrderService.savePO(productOrderDto), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> updatePo(@RequestBody ProductOrderDto productOrderDto) {
        productOrderService.updatePo(productOrderDto);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/order/{id}")
    public ResponseEntity<ProductOrderDto> deletePOByOrderId(@PathVariable("id") Integer orderId) {
        productOrderService.deletePoByOrdersId(orderId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/order/{orderId}/product/{productId}")
    public ResponseEntity<Void> deletePOByOrdersAndProduct(@PathVariable("orderId") Integer orderId,@PathVariable("productId") Integer productId) {
        productOrderService.deletePOByOrderIdAndProductId(orderId,productId);
        return ResponseEntity.noContent().build();
    }
}
