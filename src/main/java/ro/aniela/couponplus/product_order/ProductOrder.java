package ro.aniela.couponplus.product_order;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ro.aniela.couponplus.orders.Orders;
import ro.aniela.couponplus.product.Product;

@Entity(name = "ProductOrder")
@Table(name = "product_order")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductOrder {
    @EmbeddedId
    private ProductOrderId productOrderId;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(value = "orderId")
    private Orders orders;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId(value = "productId")
    private Product product;
    @Column(name = "quantity", nullable = false)
    private Double quantity;
}
