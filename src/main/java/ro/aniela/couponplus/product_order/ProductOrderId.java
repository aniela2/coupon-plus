package ro.aniela.couponplus.product_order;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.couponplus.product.Product;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ProductOrderId {
    @Column(name = "orders_id",nullable = false)
    private Integer ordersId;
    @Column(name = "product_id",nullable = false)
    private Integer productId;

}
