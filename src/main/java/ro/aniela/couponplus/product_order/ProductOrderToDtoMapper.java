package ro.aniela.couponplus.product_order;


import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ProductOrderToDtoMapper implements Function<ProductOrder, ProductOrderDto> {

    @Override
    public ProductOrderDto apply(ProductOrder productOrder) {
        return new ProductOrderDto(productOrder.getOrders().getId(), productOrder.getProduct().getId(), productOrder.getQuantity());
    }
}
