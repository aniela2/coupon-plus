package ro.aniela.couponplus.product_order;

import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import ro.aniela.couponplus.orders.OrdersRepository;
import ro.aniela.couponplus.product.ProductRepository;

import java.util.List;

@Service
@RequiredArgsConstructor

public class ProductOrderService {
    private final ProductOrderRepository productOrderRepository;
    private final ProductOrderToDtoMapper productOrderToDtoMapper;
    private final OrdersRepository ordersRepository;
    private final ProductRepository productRepository;

    public List<ProductOrderDto> getPoByOrder(Integer ordersId) {
        List<ProductOrder> po = productOrderRepository.productOrdersByOrderId(ordersId);
        return po.stream().map(productOrderToDtoMapper).toList();
    }

    public List<ProductOrderDto> getPoByProduct(Integer productId) {
        return productOrderRepository.productOrderByProductId(productId);
    }

    public ProductOrderDto getPoByOrderAndProduct(Integer orderId, Integer productId) {
        return productOrderRepository.getPoByOrderIdAndProductId(orderId, productId);
    }

    public List<ProductOrderDto> getAllPo() {
        return productOrderRepository.getAllPO().stream().map(po -> productOrderToDtoMapper.apply(po)).toList();
    }

    public ProductOrderDto savePO(ProductOrderDto productOrderDto) {
        ProductOrder po = new ProductOrder();
        ProductOrderId poi = new ProductOrderId(productOrderDto.orderId(), productOrderDto.productId());
        po.setProductOrderId(poi);
        po.setOrders(ordersRepository.getReferenceById(productOrderDto.orderId()));
        po.setProduct(productRepository.getReferenceById(productOrderDto.productId()));
        po.setQuantity(productOrderDto.quantity());
        productOrderRepository.save(po);
        return productOrderToDtoMapper.apply(po);
    }

    public void updatePo(ProductOrderDto productOrderDto) {
        productOrderRepository.updateProductOrdersQuantity(productOrderDto.orderId(), productOrderDto.productId(), productOrderDto.quantity());
    }

    public void deletePoByOrdersId(Integer orderId) {
        productOrderRepository.deleteProductOrderByOrdersId(orderId);
    }

//    public void deletePOByOrdersAndProduct(Integer orderId, Integer productId) {
//        productOrderRepository.getProductOrderByOrdersIdAndProductId(orderId, productId);
//    }

    void deletePOByOrderIdAndProductId( Integer orderId, Integer productId) {
        productOrderRepository.deletePOByOrderIdAndProductId(orderId, productId);
    }

}
