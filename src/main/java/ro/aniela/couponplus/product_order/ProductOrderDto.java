package ro.aniela.couponplus.product_order;

public record ProductOrderDto(Integer orderId, Integer productId, Double quantity) {

}
