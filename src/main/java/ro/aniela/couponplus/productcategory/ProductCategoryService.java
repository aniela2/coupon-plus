package ro.aniela.couponplus.productcategory;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductCategoryService {
    private final ProductCategoryRepository productCategoryRepository;
    private final ProductCategoryToDtoMapper productCategoryToDtoMapper;

    public ProductCategoryDto getProductCategoryById(Integer id) {
        return productCategoryToDtoMapper.apply(productCategoryRepository.getReferenceById(id));
    }

    public List<ProductCategoryDto> getAllProductCategory() {
        return productCategoryRepository.findAll().stream().map(pc -> productCategoryToDtoMapper.apply(pc)).toList();
    }

    public ProductCategoryDto saveProductCategory(ProductCategoryDto productCategoryDto) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setType(productCategoryDto.type());
        productCategory.setName(productCategoryDto.name());
        productCategoryRepository.save(productCategory);
        return productCategoryToDtoMapper.apply(productCategory);
    }

    public ProductCategoryDto updateProductCategory(Integer id, ProductCategoryDto productCategoryDto) {
       ProductCategory productCategory= productCategoryRepository.getReferenceById(id);
        productCategory.setType(productCategoryDto.type());
        productCategory.setName(productCategoryDto.name());
        productCategoryRepository.save(productCategory);
        return productCategoryToDtoMapper.apply(productCategory);
    }

    public void deleteProductCategory(Integer id) {
        productCategoryRepository.deleteById(id);
    }
}
