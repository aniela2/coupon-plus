package ro.aniela.couponplus.productcategory;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/product-categories")
@RequiredArgsConstructor
public class ProductCategoryController {
    private final ProductCategoryService productCategoryService;

    @GetMapping("/{id}")
    public ResponseEntity<ProductCategoryDto> getPDById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(productCategoryService.getProductCategoryById(id));
    }

    @GetMapping
    public ResponseEntity<List<ProductCategoryDto>> getAllPD() {
        return ResponseEntity.ok(productCategoryService.getAllProductCategory());
    }

    @PostMapping
    public ResponseEntity<ProductCategoryDto> savePD(@RequestBody ProductCategoryDto productCategoryDto) {
        return new ResponseEntity(productCategoryService.saveProductCategory(productCategoryDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductCategoryDto> updateProductCat(@PathVariable("id") Integer id, @RequestBody ProductCategoryDto productCategoryDto) {
        return ResponseEntity.ok(productCategoryService.updateProductCategory(id, productCategoryDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProdCatById(@PathVariable("id") Integer id) {
        productCategoryService.deleteProductCategory(id);
        return ResponseEntity.noContent().build();
    }


}
