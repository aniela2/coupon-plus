package ro.aniela.couponplus.productcategory;

public record ProductCategoryDto(Integer id, String type, String name) {
}
