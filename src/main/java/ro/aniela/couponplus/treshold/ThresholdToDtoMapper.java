package ro.aniela.couponplus.treshold;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class ThresholdToDtoMapper implements Function<Threshold, ThresholdDto> {
    public ThresholdDto apply(Threshold threshold){
        return new ThresholdDto(threshold.getId(), threshold.getName(), threshold.getStartValue(), threshold.getEndValue());
    }
}
