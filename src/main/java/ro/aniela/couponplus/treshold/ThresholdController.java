package ro.aniela.couponplus.treshold;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/thresholds")
@RequiredArgsConstructor
public class ThresholdController {
    private final ThresholdService thresholdService;

    @GetMapping("/{id}")
    public ResponseEntity<ThresholdDto> getThresholdById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(thresholdService.getThresholdById(id));
    }

    @GetMapping
    public ResponseEntity<List<ThresholdDto>> getAllThreshold() {
        return ResponseEntity.ok(thresholdService.getAllThreshold());
    }

    @PostMapping
    public ResponseEntity<ThresholdDto> saveThreshold(@RequestBody ThresholdDto thresholdDto) {
        return new ResponseEntity(thresholdService.saveThreshold(thresholdDto), HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ThresholdDto> updateTh(@PathVariable("id") Integer id, @RequestBody ThresholdDto thresholdDto){
        return ResponseEntity.ok(thresholdService.updateThreshold(id, thresholdDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteThreshold(@PathVariable("id") Integer id) {
        thresholdService.deleteThresholdById(id);
        return ResponseEntity.noContent().build();
    }
}
