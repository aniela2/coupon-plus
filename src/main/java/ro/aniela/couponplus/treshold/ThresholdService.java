package ro.aniela.couponplus.treshold;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ThresholdService {
    private final ThresholdRepository thresholdRepository;
    private final ThresholdToDtoMapper thresholdToDtoMapper;

    public ThresholdDto getThresholdById(Integer id) {
        return thresholdToDtoMapper.apply(thresholdRepository.getReferenceById(id));
    }

    public List<ThresholdDto> getAllThreshold() {
        return thresholdRepository.findAll().stream().map(t -> thresholdToDtoMapper.apply(t)).toList();
    }

    public ThresholdDto saveThreshold(ThresholdDto thresholdDto) {
        Threshold threshold = new Threshold();
        threshold.setName(thresholdDto.name());
        threshold.setStartValue(thresholdDto.startValue());
        threshold.setEndValue(thresholdDto.endValue());
        thresholdRepository.save(threshold);
        return thresholdToDtoMapper.apply(threshold);
    }
    public ThresholdDto updateThreshold(Integer id, ThresholdDto thresholdDto){
        Threshold threshold= thresholdRepository.getReferenceById(id);
        threshold.setName(thresholdDto.name());
        threshold.setStartValue(thresholdDto.startValue());
        threshold.setEndValue(thresholdDto.endValue());
        thresholdRepository.save(threshold);
       return thresholdToDtoMapper.apply(threshold);
    }
    public void deleteThresholdById(Integer id){
         thresholdRepository.deleteById(id);
    }

}
