package ro.aniela.couponplus.treshold;

public record ThresholdDto(Integer id, String name, Double startValue, Double endValue) {
}
