package ro.aniela.couponplus.treshold;

import jakarta.persistence.*;
import lombok.*;
import ro.aniela.couponplus.threshold_level2.Level2ThresholdL2;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "Threshold")
@Table(name = "threshold")

public class Threshold {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "start_value",nullable = false)
    private Double startValue;
    @Column(name = "end_value", nullable = false)
    private Double endValue;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "threshold")
    private List<Level2ThresholdL2> thresholL2dList;
}

