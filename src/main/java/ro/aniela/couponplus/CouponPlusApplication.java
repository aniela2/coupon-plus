package ro.aniela.couponplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CouponPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(CouponPlusApplication.class, args);
    }

}
