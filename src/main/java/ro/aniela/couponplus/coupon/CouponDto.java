package ro.aniela.couponplus.coupon;

public record CouponDto(Integer id, Integer buyerId) {
}
