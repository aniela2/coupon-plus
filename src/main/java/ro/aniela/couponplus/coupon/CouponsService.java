package ro.aniela.couponplus.coupon;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.couponplus.buyer.BuyerRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CouponsService {
    private final CouponsRepository couponsRepository;
    private final CouponsToDtoMapper couponsToDtoMapper;
    private final BuyerRepository buyerRepository;

    public CouponDto getCouponsById(Integer id) {
        return couponsToDtoMapper.apply(couponsRepository.getReferenceById(id));
    }

    public List<CouponDto> getAllCoupons() {
        return couponsRepository.findAll().stream().map(c -> couponsToDtoMapper.apply(c)).toList();
    }

    public CouponDto saveCoupon(CouponDto couponDto) {
        Coupon coupon = new Coupon();
        coupon.setBuyer(buyerRepository.getReferenceById(couponDto.buyerId()));
        couponsRepository.save(coupon);
        return couponsToDtoMapper.apply(coupon);
    }

    public CouponDto updateCoupon(Integer id, CouponDto couponDto) {
        Coupon coupon = couponsRepository.getReferenceById(id);
        coupon.setBuyer(buyerRepository.getReferenceById(couponDto.buyerId()));
        couponsRepository.save(coupon);
        return couponsToDtoMapper.apply(coupon);
    }

    public void deleteCouponById(Integer id) {
        couponsRepository.deleteById(id);
    }


}
