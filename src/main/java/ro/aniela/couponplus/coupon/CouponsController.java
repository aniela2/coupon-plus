package ro.aniela.couponplus.coupon;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/coupons")
@RequiredArgsConstructor
public class CouponsController {
    public final CouponsService couponsService;

    @GetMapping("/{id}")
    private ResponseEntity<CouponDto> getCouponById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(couponsService.getCouponsById(id));
    }

    @GetMapping
    private ResponseEntity<List<CouponDto>> getAllCoupon() {
        return ResponseEntity.ok(couponsService.getAllCoupons());
    }

    @PostMapping
    private ResponseEntity<CouponDto> saveCoupon(@RequestBody CouponDto couponDto) {
        return new ResponseEntity(couponsService.saveCoupon(couponDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<CouponDto> updateCoupon(@PathVariable("id") Integer id, @RequestBody CouponDto couponDto) {
        return ResponseEntity.ok(couponsService.updateCoupon(id, couponDto));
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteCouponById(@PathVariable("id") Integer id) {
        couponsService.deleteCouponById(id);
        return ResponseEntity.noContent().build();
    }
}
