package ro.aniela.couponplus.coupon;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class CouponsToDtoMapper implements Function<Coupon, CouponDto> {
    public CouponDto apply(Coupon coupon) {
        return new CouponDto(coupon.getId(), coupon.getBuyer().getId());
    }
}
